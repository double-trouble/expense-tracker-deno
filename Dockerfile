FROM hayd/alpine-deno:1.2.0

EXPOSE 8000

WORKDIR /app

USER deno

COPY deps.ts .
COPY lock-file.json .
RUN deno cache deps.ts --lock=lock-file.json

ADD . .

CMD ["run", "--allow-net", "--allow-read", "--allow-env", "mod.ts"]
