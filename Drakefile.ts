import { desc, run, task, readFile, shCapture, sh, abort } from "./deps.ts";

desc("Verify Deno Version");
task("version-check", [], async function () {
  const expectedVersion = readFile(".version");
  const { output } = await shCapture("deno -V");
  if (output != expectedVersion) {
    abort("Invalid Deno Version!");
  }
});

desc("deno cache");
task("pull-cache", [], async function () {
  await sh("deno cache deps.ts --lock=lock-file.json");
});

desc("deno fmt");
task("fmt", ["pull-cache"], async function () {
  const { code } = await shCapture("deno fmt --check");
  if (code != 0) {
    abort(`Run \`deno fmt\` and commit the changes to proceed.`);
  }
});

desc("deno lint --unstable");
task("lint", ["fmt"], async function () {
  const { code } = await shCapture("deno lint --unstable");
  if (code != 0) {
    abort(`Run \`deno lint --unstable\` and fix the issues to proceed.`);
  }
});

desc("Checking for uncommitted changes");
task("git-repo-check", ["lint"], async function () {
  const { output, error } = await shCapture(
    'git status --porcelain=v1 2>/dev/null | wc -l | tr "\n" " "',
  );
  if (output != "0 ") {
    abort(
      `${output}uncommitted file changes found. Please commit them to proceed.`,
    );
  }
});

desc("Static Analysis");
task("sa", ["git-repo-check"], async function () {
  await sh("echo Static Analysis Complete!");
});

run();
