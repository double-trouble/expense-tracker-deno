import { Application, log, send } from "./deps.ts";
import { router } from "./routes/api.ts";

const app = new Application();
const PORT = Number(Deno.env.get("PORT")) || 8000;

// Logger Setup
await log.setup({
  handlers: {
    console: new log.handlers.ConsoleHandler("INFO"),
  },
  loggers: {
    default: {
      level: "INFO",
      handlers: ["console"],
    },
  },
});

// Error Event on Oak Level
app.addEventListener("error", (event) => {
  log.error(event.error);
});

// Listen Event Logger for Oak
app.addEventListener("listen", ({ hostname, port, secure }) => {
  log.info(
    `Listening on: ${secure ? "https://" : "http://"}${hostname ??
      "localhost"}:${port}`,
  );
});

// Error handler Middleware
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.response.body = "Internal Server Error!";
    throw err;
  }
});

// Logger Middleware
app.use(async (ctx, next) => {
  await next();
  log.info(
    `${ctx.request.method} ${ctx.request.url} : ${
      ctx.response.headers.get("X-Response-Time")
    }`,
  );
});

// Response Time  Benchmarking
app.use(async (ctx, next) => {
  const startTime = Date.now();
  await next();
  const delta = Date.now() - startTime;
  ctx.response.headers.set("X-Response-Time", `${delta} ms`);
});

app.use(router.routes());
app.use(router.allowedMethods());

// Serving Static Files for Frontend
app.use(async (ctx) => {
  const filePath = ctx.request.url.pathname;
  const fileWhitelist = [
    "/index.html",
    "/images/favicon.png",
    "/javascripts/script.js",
    "/stylesheets/style.css",
  ];

  if (fileWhitelist.includes(filePath)) {
    await send(ctx, filePath, {
      root: `${Deno.cwd()}/public`,
    });
  }
});

if (import.meta.main) {
  await app.listen({ port: PORT });
}
